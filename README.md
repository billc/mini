# Mini
Experimental link shorten web app using Phoenix, VueJS, and Bulma.io


Features
  * Single page application uses Vuejs to dynamically request to "minify" links submitted
  * Includes GenServer processes to extract metadata asynchronously from destination. 
  * User interface is asynchronously updates UI with metadata extracted through Phoenix sockets
 
 
Requirements
  * Elixir 1.4.1
  * Phoenix Framework
  * VueJS 2.0
  * Bulma
  * Postgresql
 
 
To run
  * Mini-redirect project for actual following minified links
  * Nginx for port forwarding
  * Update to etc/hosts to bypass http://mi.in to localhost


For information on the components, please referene the whiteboard architecture-diagram.JPG




DISCLAIMER
This is an experimental project and not intended for purposes outside of educational, non-professional needs. Source code is available under the MIT licenses with no warranty or liability.
