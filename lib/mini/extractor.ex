defmodule Mini.Extractor do
  @moduledoc """
  Documentation for Mini.Extractor.
  """

  use GenServer

  require Logger

  alias Mini.Metadata

  @timeout 60000

  def start_link(_options) do
    GenServer.start_link(__MODULE__, :ok, [])
  end

  def init(_options) do
    {:ok, []}
  end

  @doc """
  Retrieve page with the given URL, extract metadata, and post metadata to given webhook address given.
  """
  def extract({source_url, webhook}) do
    :poolboy.transaction(:mini_extractor_pool, &(GenServer.cast(&1, { :extract, {source_url, webhook} })), @timeout)
  end

  @doc """
  Given an html page, parses the title and description from the contents.

  title: found by the <title> element
  description: found by the <meta name="description"> content attribute
  """
  def parse(html) do
    %Metadata{
      title: parse_title(html),
      description: parse_description(html)
    }
  end

  defp parse_title(html) do
    case Floki.find(html, "title") do
      [{_, _, [title]} | _] -> title
      [] -> ""
    end
  end

  defp parse_description(html) do
    with [meta_name] <- Floki.find(html, "meta[name=description]"),
         [content] <- Floki.attribute(meta_name,"content")
      do
      content
      else
        # Failed to find description in expected meta tag
        _ -> ""
    end

  end

  defp post_metadata(metadata, webhook) do
    case HTTPoison.patch webhook, encode(metadata), [{"Content-Type", "application/json;"}] do
      {:ok, %HTTPoison.Response{status_code: 200} = response} ->
        # Logger.debug response.body
        Logger.debug "Successfully extracted and posted metadata for #{webhook}"
      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.warn "Unable to extract and post metadata for #{webhook}"
    end

  end

  defp encode(metadata) do
    %{
      "link":
      %{
        "title": metadata.title,
        "description": metadata.description
      }
    }
    |> Poison.encode!
  end

  #
  ### Server APIs
  #

  def handle_cast({:extract, {url, webhook}}, state) do
    case HTTPoison.get(url, [], [follow_redirect: true]) do
      {:ok, %HTTPoison.Response{body: response}} ->
        response
        |> parse
        |> post_metadata(webhook)
      {:error, error} ->
        Logger.error "Unable to process request -> #{error.reason}"
    end
    {:noreply, state}
  end
end
