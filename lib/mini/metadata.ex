defmodule Mini.Metadata do
  defstruct title: nil, description: nil
end

defimpl String.Chars, for: Mini.Metadata do
  def to_string(metadata) do
    "{\"title\": #{metadata.title}, \"description\": #{metadata.description}}"
  end
end
