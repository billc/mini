defmodule Mini.Repo.Migrations.CreateLink do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :address, :string
      add :location, :string

      timestamps()
    end

  end
end
