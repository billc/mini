defmodule Mini.Repo.Migrations.AddMetadataFieldsToLinks do
  use Ecto.Migration

  def change do
    alter table(:links) do
      add :title, :string
      add :description, :string
    end

  end
end
