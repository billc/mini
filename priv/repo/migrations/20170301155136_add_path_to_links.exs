defmodule Mini.Repo.Migrations.AddPathToLinks do
  use Ecto.Migration

  def change do
    alter table(:links) do
      add :path, :string
    end
  end
end
