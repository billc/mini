defmodule Mini.Factory do
  use ExMachina.Ecto, repo: Mini.Repo

  def link_factory do
    %Mini.Link{
      address: "g",
      location: "http://www.google.com"
    }
  end
end
