defmodule Mini.RoomChannel do
  use Mini.Web, :channel

  def join("room:lobby", _payload, socket) do
    {:ok, socket}
  end

  def join("room:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "Unauthorized"}}
  end

  def handle_in("new_mini", %{"body" => body}, socket) do
    broadcast! socket, "new_mini", %{body: body}
    {:noreply, socket}
  end

  def handle_out("new_mini", payload, socket) do
    push socket, "new_mini", payload
    {:noreply, socket}
  end

  def broadcast_change(link) do
    payload = Mini.LinkView.render("link.json", link: link)
    Mini.Endpoint.broadcast("room:lobby", "new_mini", payload)
  end
end
