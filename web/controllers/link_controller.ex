defmodule Mini.LinkController do
  use Mini.Web, :controller

  alias Mini.Link
  alias Mini.Extractor

  require Logger

  @host "http://mi.ni"

  def index(conn, _params) do
    links = Repo.all(Link)
    render(conn, "index.json", links: links)
  end

  def create(conn, %{"link" => link_params}) do
    location = link_params["location"]
    path = "/" <> minify_location(location)

    link = %Link{"address": @host <> path, "path": path}
    changeset = Link.changeset(link, link_params)

    case Repo.insert(changeset) do
      {:ok, link} ->
        Extractor.extract( {location, link_url(conn, :update, link)} )
        conn
        |> put_status(:created)
        |> put_resp_header("location", link_path(conn, :show, link))
        |> render("show.json", link: link)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Mini.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    link = Repo.get!(Link, id)
    render(conn, "show.json", link: link)
  end

  def update(conn, %{"id" => id, "link" => link_params}) do
    link = Repo.get!(Link, id)
    changeset = Link.changeset(link, link_params)

    case Repo.update(changeset) do
      {:ok, link} ->
        Mini.RoomChannel.broadcast_change(link)
        render(conn, "show.json", link: link)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Mini.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    link = Repo.get!(Link, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(link)

    send_resp(conn, :no_content, "")
  end

  defp minify_location(url) do
    UUID.uuid5(:url, url, :hex)
    |> :erlang.phash2
    |> Integer.to_string(16)

  end
end
