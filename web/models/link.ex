defmodule Mini.Link do
  use Mini.Web, :model

  schema "links" do
    field :path, :string
    field :address, :string
    field :location, :string
    field :title, :string
    field :description, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:address, :location, :title, :description])
    |> validate_required([:address, :location])
  end
end
