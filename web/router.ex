defmodule Mini.Router do
  use Mini.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Plug.Logger, log: :debug
  end

  scope "/", Mini do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api", Mini do
    pipe_through :api
    resources "/links", LinkController, except: [:new, :edit]
  end
end
