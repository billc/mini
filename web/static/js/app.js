// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"

// Brunch configuration is loading vue standalone in globals
// importing Vue in JS unnecessary
// import Vue from 'vue'
import axios from 'axios'
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);


var app = new Vue({
    el: '#app',

    data: {
        url: '',
        previous: [],
        channel: null,
    },

    mounted() {
        this.channel = socket.channel("room:lobby", {});
        this.channel.on("new_mini", payload => {
            let index = this.previous.map(function (p) { return p.id; }).indexOf(payload.id);
            if (index > -1) {
                // this.$set(this.previous, index, payload)
                this.previous[index].title = payload.title;
                this.previous[index].description = payload.description;
                console.log("Updated metadata for " + this.previous[index].location)
            }
        });

        this.channel.join()
            .receive("ok", response => { console.log("Joined successfully", response)})
            .receive("error", response => { console.log("unable to join", response)})
    },

    methods: {
        validateBeforeSubmit(e) {
            this.$validator.validateAll();

            if (!this.errors.any()) {
                this.submitForm()
            }
        },
         
        submitForm: function () {
            this.errors.clear;

            axios.post('/api/links', {
                link : {
                    location: this.url
                }
            })
                .then( function (response) {
                    app.url = ''
                    app.previous.unshift(response.data.data)

                    console.log(response)
                })
                .catch( function (error) {
                    console.log(error)
                });

        }
    }

});
