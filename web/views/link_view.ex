defmodule Mini.LinkView do
  use Mini.Web, :view

  def render("index.json", %{links: links}) do
    %{data: render_many(links, Mini.LinkView, "link.json")}
  end

  def render("show.json", %{link: link}) do
    %{data: render_one(link, Mini.LinkView, "link.json")}
  end

  def render("link.json", %{link: link}) do
    %{id: link.id,
      path: link.path,
      address: link.address,
      location: link.location,
      title: link.title,
      description: link.description}
  end
end
